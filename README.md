##Tuturial configuração ambiente Windows

IDE utilizada no projeto Visual Studio Code

##Download terminal cmder

Realizar o Download cmder (https://cmder.net/)

Descompactar e mover para C:\tools

Navegar até o diretório C:\tools\cmder\vendor\git-for-windows\usr\bin, pegar o caminho e inserir nova variável de sistema nome: cmder
e o caminho C:\tools\cmder\vendor\git-for-windows\usr\bin

Navegar ate o diretório C:\tools\cmder selercionar o arquivo Cmder e criar um atalho na área de trabalho

##Ambiente
Fazer o download e instalar o ruby https://rubyinstaller.org/downloads/ (versão estável com devkit),  
fechar prompts de comando abertos antes da instalação

Adicionar variável de ambiente ruby  no path, o valor deve ser o caminho da instalação do ruby, ex: C:\Ruby25-x64\bin

Configurar Dev kit, abra o cmder e rode o comando ridk install (escolher opção 3)

Rodar o comando gem install bundler

Criar pasta em diretório na raiz c: e navegar ate ela através do cmder

Com o cmder aberto Clonar o projeto git clone https://rglmerlo@bitbucket.org/rglmerlo/mazza-prova.git

Rodar bundle install, irá instalar as dependencias configuradas no Gemfile

##Subindo navegador
Realize o download do driver firefox em: https://github.com/mozilla/geckodriver/releases, baixe a versão correspondente ao sistema operacional/processador
Descompacte o arquivo, copie o executável, navegue até o diretório onde foi instalado o cmder C:\Tools, crie uma pasta chamada "selenium"
e cole dentro dessa pasta do driver do firefox
Adicione a variavél de ambiente C:\Tools\selenium

##Execução e report
Para rodar os testes e gerar o relatório rode o comando: rspec --format html --out report.html

Será gerado o report.hml dentro da pasta do projeto podendo ser localizada nagevando dentro da pasta do projeto ou pela propria ide

##Lista de cenários está localizada dentro da pasta DOC


##Screenshots serão salvos na pasta log caso o cenário falhe