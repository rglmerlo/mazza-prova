

describe 'Criação de conta' do
    before(:each) do
        visit 'http://automationpractice.com/index.php?controller=authentication&back=my-account'
    end

    it 'Realizar tentativa de cadastro sem preencher o campo com nenhuma informação' do

        click_button 'SubmitCreate'

        expect(find('#create_account_error')).to have_content 'Invalid email address'

    end
    it 'Realizar cadastro com caracteres inválidos' do

        fill_in 'email_create', with: '315811#$!@#'
        click_button 'SubmitCreate'

        expect(find('#create_account_error')).to have_content 'Invalid email address'

    end

    it 'Realizar cadastro utilizando dados corretos' do
        
        #cenário irá falhar pois já há um cadastro com o e-mail utilizado no teste, será gerado uma evidência dentro da pasta log
        fill_in 'email_create', with: 'rodrigo.g.merlo@gmail.com'

        click_button 'SubmitCreate'
        page.has_text?('Your personal information')
        
    end
    it 'Realizar cadastro com e-mail já cadastrado ' do

        fill_in 'email_create', with: 'a@hotmail.com'
        click_button 'SubmitCreate'

        expect(find('#create_account_error')).to have_content 'An account using this email address has already been registered'
    end

    it 'Valida campos obrigatórios', :mandatory do
        
        fill_in 'email_create', with: 'rodrigo.g.merlo@gmail.com'

        click_button 'SubmitCreate'
        click_button 'submitAccount'
        
        expect(page).to have_content 'You must register at least one phone number.'
        expect(page).to have_content 'lastname is required'
        expect(page).to have_content 'firstname is required'
        expect(page).to have_content 'passwd is required'
        expect(page).to have_content 'address1 is required'
        expect(page).to have_content 'city is required'
        expect(page).to have_content "The Zip/Postal code you've entered is invalid. It must follow this format: 00000"
        expect(page).to have_content 'This country requires you to choose a State'
    end

    it 'Valida cadastro com sucesso', :sucesso do
        #alterar email antes de criar
        fill_in 'email_create', with: 'agaga@testsm.com'

        click_button 'SubmitCreate'
        sleep 2
        choose('id_gender1', visible: false)
        fill_in 'customer_firstname', with: 'rodrigo'
        fill_in 'customer_lastname', with: 'merlo'
        fill_in 'passwd', with: 123456
        select "25", :from => "days", visible: false
        select "March", :from => "months", visible: false
        select "1982", :from => "years", visible: false
        check('newsletter', visible: false)
        fill_in 'firstname', with: 'rodrigo'
        fill_in 'lastname', with: 'merlo'
        fill_in 'company', with: 'teste'
        fill_in 'address1', with: 'rua alameda vital'
        fill_in 'city', with: 'Teste'
        select "New York", :from => "id_state", visible: false
        fill_in 'postcode', with: '13800'
        fill_in 'phone_mobile', with: '199998745'
        fill_in 'alias', with: 'Home'
        click_button 'submitAccount'
        sleep 2
        expect(page).to have_content 'Welcome to your account. Here you can manage all of your personal information and orders'
    end


end
