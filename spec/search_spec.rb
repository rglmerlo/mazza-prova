

describe 'Campo de busca' do
    before(:each) do
        visit 'http://automationpractice.com/index.php'
    end
    it 'Realizar busca sem preenchimento de informação' do

        click_button 'Search'

        expect(find('.alert-warning')).to have_content 'Please enter a search keyword'
        expect(find('.heading-counter')).to have_content '0 results have been found.'
    end

    it 'Realizar busca com caracteres inválidos' do

        fill_in 'search_query_top', with: '315811#$!@#'
        click_button 'Search'

        expect(find('.alert-warning')).to have_content 'Please enter a valid search keyword'
        expect(find('.heading-counter')).to have_content '0 results have been found.'
    end

    it 'Realizar busca com critérios válidos' do
        
        fill_in 'search_query_top', with: 'dress'
        click_button 'Search'

        expect(find('.heading-counter')).to have_content 'results have been found'
    end

    it 'Realizar checagem se campo de busca está disponível na página' do

        expect(page).to have_field("search_query_top")
    end

    it 'Realizar busca com cor + categoria' do

        fill_in 'search_query_top', with: 'orange dress'
        click_button 'Search'

        expect(page).to have_selector('#color_21', visible: true)
        
    end

    it 'Realizar busca com data e categoria' do

        fill_in 'search_query_top', with: '07/24/2019 dress'
        click_button 'Search'

        expect(find('.alert-warning')).to have_content 'No results were found for your search "07/24/2019 dress'
        expect(find('.heading-counter')).to have_content '0 results have been found.'
    end
end
