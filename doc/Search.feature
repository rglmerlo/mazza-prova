    Estória: Campo de busca
        Sendo um usuário não registrado
        Eu quero procurar por itens de minha escolha

        
    Cenário: Realizar busca sem preenchimento de informação
        Dado que eu acesse o site http://automationpractice.com/index.php
        Quando eu clicar no botão Search
        Então o sistema não deve retornar nenhum resultado
        E informar ao usuário que ele deve preencher uma palavra chave

    Cenário: Realizar busca com caracteres inválidos
        Dado que eu acesse o site http://automationpractice.com/index.php
        E preencha o campo de busca com caracteres inválidos '315811#$!@#'
        Quando eu clicar no botão Search
        Então o sistema deve retornar a mensagem de alerta 'No results were found for your search "315811#$!@#"'
        E exibir a mensagem '0 results have been found'

    Cenário: Realizar busca com critérios válidos
        Dado que eu acesse o site http://automationpractice.com/index.php
        E preencha o campo de busca com com a informação 'dress'
        Quando eu clicar no botão Search
        Então o sistema deve retornar a mensagem de alerta 'results have been found'

    Cenário: Realizar busca com critérios válidos e validar sugestão de busca
        Dado que eu acesse o site http://automationpractice.com/index.php
        E preencha o campo de busca com com a informação 'dress'
        Quando a sugestão de busca for exibida na tela
        Então o sistema não deve exibir nenhum outro resultado diferente do utilizado no critério de busca

    Cenário: Realizar busca com critérios válidos e validar link da sugestão
        Dado que eu acesse o site http://automationpractice.com/index.php
        E preencha o campo de busca com com a informação 'dress'
        E a sugestão de busca for exibida na tela
        Quando eu clicar no primeiro link
        Então o sistema deve me redirecionar ao produto do link corretamente

    Cenário: Realizar checagem se campo de busca está disponível na página
        Dado que eu acesse o site http://automationpractice.com/index.php
        Quando a página for carregada
        Então o o campo busca deve estar disponível na página

    Cenário: Realizar busca com a palavra vazia no campo de Busca
        Dado que eu acesse o site http://automationpractice.com/index.php
        E a página for carregada
        Quando eu inserir o valor "" no campo de busca
        E clicar no botão search
        Então sistema deve retornar a mensagem No results were found for your search

    Cenário: Realizar busca com cor + categoria
        Dado que eu acesse o site http://automationpractice.com/index.php
        E a página for carregada
        Quando eu inserir o valor orange dress no campo de busca
        E clicar no botão search
        Então o o campo busca deve retornar os produtos na categoria estando disponiveis na cor solicitada

    Cenário: Realizar busca com data e categoria
        Dado que eu acesse o site http://automationpractice.com/index.php
        E a página for carregada
        Quando eu inserir o valor 07/24/2019 dress 
        E clicar no botão search
        Então sistema deve retornar a mensagem No results were found for your search
